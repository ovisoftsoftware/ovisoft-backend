<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\CompanyDetail;

class Company extends Model
{
	use HasFactory;

	public function details()
  {
    return $this->hasMany(CompanyDetail::class);
  }

	protected $fillable = [
		'title',
		'description'
	];

}
