<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Company;


class CompanyDetail extends Model
{
  use HasFactory;

  public function company()
  {
    return $this->belongsTo(Company::class);
  }

  protected $fillable = [
    'company_id',
		'description',
		'image'
  ];

}
