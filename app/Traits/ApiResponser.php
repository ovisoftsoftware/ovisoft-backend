<?php

namespace App\Traits;

use phpDocumentor\Reflection\Types\Boolean;

trait ApiResponser
{
  protected function success(bool $status, string $message, $data, int $code= 200){
    
    return response()->json([
      'status' => $status,
      'message' => $message,
      'data' => $data,
    ], $code);

  }

  protected function error(bool $status, string $message, int $code)
  {
    return response()->json([
      'status' => $status,
      'message' => $message,
    ], $code);
  }

}