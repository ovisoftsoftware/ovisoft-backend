<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContactRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      // 'title' => 'required|string|unique:contacts,title,'.$id,
      'title' => [
        'required',
        'string',
        'max:255',
        Rule::unique('contacts', 'title')->ignore($this->contact)
      ],
			'description' => 'required|string',
			'url_contact' => 'required|string|url',
    ];
  }
}
