<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Http\Requests\CompanyDetailRequest;
use App\Models\CompanyDetail;

class CompanyDetailController extends Controller
{

  use ApiResponser;

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\CompanyDetailRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(CompanyDetailRequest $request)
  {
    $company_detail = CompanyDetail::create($request->all());

    return $this->success(True, 'pagina de detalles de quienes somos creada', $company_detail, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $company_detail = CompanyDetail::find($id);

    if (!$company_detail) {
      return $this->error(False, 'La compania con el id '. $id .'no existe', 400);
    }

    return $this->success(True, 'pagina de detalles de la empresa', $company_detail);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\CompanyDetailRequest  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(CompanyDetailRequest $request, $id)
  {
    $company_detail = CompanyDetail::find($id);

    if (!$company_detail) {
      return $this->error(False, 'La compania con el id '. $id .'no existe', 400);
    }
    
    $company_detail->update($request->all());

    return $this->success(True, 'pagina de los detalles de la compania actualizada', $company_detail);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $company_detail = CompanyDetail::find($id);

    if (!$company_detail) {
      return $this->error(False, 'La compania con el id '. $id .'no existe', 400);
    }
    
    $company_detail->detele();

    return response()->json([
      'status' => true,
      'message' => 'eliminado existosamente',
    ], 200);
  }
}
