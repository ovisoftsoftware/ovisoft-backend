<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DownloadRequest;
use App\Traits\ApiResponser;
use App\Models\Download;

class DownloadController extends Controller
{
  use ApiResponser;
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $downloads = Download::get();

    return $this->success(true,'lista de archivos de descargas', $downloads);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\DownloadRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(DownloadRequest $request)
  {
    $download = Download::create($request->all());

    return $this->success(true, 'archivo de descarga creada', $download, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $download = Download::find($id);

    if (!$download) {
      return $this->error(false, 'archivo de descarga con el id '. $id. ' no existe', 400);
    }

    return $this->success(true, 'archivo de descarga encontrado', $download);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\DownloadRequest  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(DownloadRequest $request, $id)
  {
    $download = Download::find($id);

    if (!$download) {
      return $this->error(false, 'archivo de descarga con el id '. $id. ' no existe', 400);
    }

    $download->update($request->all());

    return $this->success(true, 'archivo de descarga actualizado', $download);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $download = Download::find($id);

    if (!$download) {
      return $this->error(false, 'archivo de descarga con el id '. $id. ' no existe', 400);
    }

    $download->delete();

    return response()->json([
      'status' => true,
      'message' => 'arhivo de descarga eliminado exitosamente',
    ], 200);
  }
}
