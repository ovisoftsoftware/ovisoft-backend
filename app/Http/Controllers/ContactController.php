<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Traits\ApiResponser;
use App\Models\Contact;

class ContactController extends Controller
{
  use ApiResponser;
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $contacts = Contact::get();

    return $this->success(true,'lista de contactos', $contacts);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\ContactRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(ContactRequest $request)
  {
    $contact = Contact::create($request->all());

    return $this->success(true,'contacto creado', $contact, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $contact = Contact::find($id);

    if (!$contact) {
      return $this->error(false,'El contacto con el id '. $id. ' no exite', 400);//400 bad request
    }

    return $this->success(true, 'contacto obtenido exitosamente', $contact);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\ContactRequest  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(ContactRequest $request, $id)
  {
    $contact = Contact::find($id);
    
    if (!$contact) {
      return $this->error(false, 'El contacto con el id '. $id. ' no existe', 400);
    }
    
    $contact->update($request->all());

    return $this->success(true, 'contacto actualizado exitosamente', $contact);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $contact = Contact::find($id);

    if (!$contact) {
      return $this->error(false,'El contacto con el id '. $id. ' no existe', 400);
    }

    $contact->delete();
    
    return response()->json([
      'status' => true,
      'message' => 'Contacto eliminado exitosamente',
    ], 200);
  }
}
