<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Http\Requests\ProductRequest;
use App\Models\Product;

class ProductController extends Controller
{
	use ApiResponser;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$products = Product::get();
		
		return $this->success(True, 'Lista de productos', $products);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\ProductRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(ProductRequest $request)
	{
		$product = Product::create($request->all());

		return $this->success(True, 'Producto creado', $product);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$product = Product::find($id);

		if (!$product) {
			return $this->error(False, 'El producto con el id '. $id. ' no existe', 400);
		}

		return $this->success(true, 'Producto encontrado', $product);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\ProductRequest  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(ProductRequest $request, $id)
	{
		$product = Product::find($id);

		if (!$product) {
			return $this->error(False, 'El producto con el id '. $id. ' no existe', 400);
		}

		$product->update($request->all());

		return $this->success(True, 'Producto actualizado exitosamente', $product);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$product = Product::find($id);

		if (!$product) {
			return $this->error(False, 'No se encontrado el producto con el id '. $id, 400);
			
		}

		$product->delete();

		return response()->json([
      'status' => true,
      'message' => ' Producto eliminado exitosamente',
    ], 200);

	}
}
