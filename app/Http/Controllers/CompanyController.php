<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Http\Requests\CompanyRequest;
use App\Models\Company;

class CompanyController extends Controller
{
  use ApiResponser;

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $company = Company::get();

    return $this->success(True,'pagina de quienes somos de la empresa', $company);

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\CompanyRequest  $request
   * @return \Illuminate\Http\Response
   */
  public function store(CompanyRequest $request)
  {
    $company = Company::create($request->all());

    return $this->success(True, 'pagina de quienes somos creada', $company, 201);    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $company = Company::find($id);

    if (!$company) {
      return $this->error(False, 'la compania con el id '. $id .'no existe', 400);
    }

    $company->details = $company->details;
    
    return $this->success(True, 'detalles de la pagina quienes somos', $company);
  }

  /**
   * Update the specified resource in storage.
   *  
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(CompanyRequest $request, $id)
  {
    $company = Company::find($id);

    if (!$company) {
      return $this->error(False, 'La compania con el id '. $id .'no existe', 400);
    }
    
    $company->update($request->all());

    return $this->success(True, 'pagina de quienes somos actualizada', $company);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $company = Company::find($id);

    if (!$company) {
      return $this->error(False, 'La compania con el id '. $id .' no existe', 400);
    }
    
    $company->delete();

    return response()->json([
      'status' => true,
      'message' => 'eliminado exitosamente',
    ], 200);
  }
}
