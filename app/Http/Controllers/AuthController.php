<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Traits\ApiResponser;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
	use ApiResponser;


	public function register(Request $request)
	{
		$request->validate([
			'name' => 'required|string|max:30',	
			'role' => 'exists:roles,name',
			'email' => 'required|string|email|unique:users,email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
			'password' => 'required|string|min:6|confirmed'
		]);

		try {

			$role = Role::where('name', $request->role)->firstOrFail();

			$user = User::create([
				'name' => $request->name,
				'role_id' => $role->id,
				'email' => $request->email,
				'password' => bcrypt($request->password)
			]);
			
			$token = $user->createToken('api-token')->plainTextToken;
	
			return $this->success(True, 'user created', [
				'user' => [$user, $user->role],
				'token' => $token
			], 201);

		} catch (\Exception $th) {
			return $this->error(False, 'Internal Server Error', 500);
		}
	}

	public function login(Request $request)
	{
	
		$request->validate([
			'email' => 'required|string|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
			'password' => 'required|string'
		]);
		
		$user = User::where('email', $request->email)->first();
		
		if (!$user) {
			return $this->error(False, 'the user '.$request->email. ' does not exists', 401);
		}elseif (!Hash::check($request->password, $user->password)) {
			return $this->error(False, 'the password is incorrect', 401);
		}

		$token = $user->createToken('api-token')->plainTextToken;

		return $this->success(True, 'authotized', [
			'user' => $user,
			'token' => $token
		], 200);
	}

	public function logout()
	{
		auth()->user()->tokens()->delete();

		return response()->json([
			'message' => 'logged out'
		], 200);
	}
}
