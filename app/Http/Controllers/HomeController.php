<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreHomeRequest;
use App\Traits\ApiResponser;
use App\Models\Home;

class HomeController extends Controller
{
    use ApiResponser;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home = Home::get();

        return $this->success(True, 'home page', $home);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Request\StoreHomeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHomeRequest $request)
    {
        $home = Home::create($request->all());

        return $this->success(True, 'home page created', $home, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Request\StoreHomeRequests  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreHomeRequest $request, $id)
    {
        $home = Home::find($id);

        if ($home) {
            $home->update($request->all());
            return $this->success(True, 'home page updated', $home);
        }else {
            return $this->error(False,'the home with id '. $id . ', does not exist', 400);
        }
    }
}
