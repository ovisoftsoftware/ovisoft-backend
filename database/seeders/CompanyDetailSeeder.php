<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\CompanyDetail;

class CompanyDetailSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $company = Company::where('title', 'historia')->first();

    $company_detail = CompanyDetail::create([
      'company_id' => $company->id,
      'description' => 'descripcion detallada de la historia',
      'image' => 'https://enriqueortegaburgos.com/wp-content/uploads/2019/10/joyas.jpg'
    ]);
  }
}
