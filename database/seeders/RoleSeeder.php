<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
            'name' => 'ADMIN_ROLE',
        ]);

        $role = Role::create([
            'name' => 'VIEW_ROLE',
        ]);
    }
}
