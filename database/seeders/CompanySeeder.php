<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $company = Company::create([
      'title' => 'mision',
      'description' => 'descripcion de la mision'
    ]);

    $company = Company::create([
      'title' => 'vision',
      'description' => 'descripcion de la vision'
    ]);

    $company = Company::create([
      'title' => 'historia',
      'description' => 'descripcion de la historia'
    ]);
  }
}
