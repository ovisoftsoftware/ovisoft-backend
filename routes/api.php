<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\CompanyDetailController;
use App\Http\Controllers\DownloadController;
use App\Http\Controllers\ContactController;

Route::post('/register', [AuthController::class,'register']);
Route::post('/login', [AuthController::class,'login']);

Route::group(['middleware' => ['auth:sanctum']], function() { 
  Route::post('/logout', [AuthController::class,'logout']);
  Route::apiResources([
    'home' => HomeController::class,
    'about-us' => CompanyController::class,
    'company-detail' => CompanyDetailController::class,
    'downloads'=> DownloadController::class,
    'products' => ProductController::class,
    'contacts' => ContactController::class,

  ], ['except' => ['index', 'show']]);
});

Route::apiResources([
  'home' => HomeController::class,
  'about-us' => CompanyController::class,
  'company-detail' => CompanyDetailController::class,
  'downloads'=> DownloadController::class,
  'products' => ProductController::class,
  'contacts' => ContactController::class,
], ['only' => ['index', 'show']]);
